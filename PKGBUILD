pkgname=hhd
pkgver=2.1.1
pkgrel=1
pkgdesc='Handheld Daemon. A tool for managing the quirks of handheld devices.'
arch=('x86_64')
url='https://github.com/hhd-dev/hhd'
license=('MIT')
depends=('python' 'python-setuptools' 'python-evdev' 'python-rich' 'python-yaml')
makedepends=('python-'{'build','installer','setuptools','wheel'})
optdepends=('hhd-ui: Electron UI for HHD')
source=("https://pypi.python.org/packages/source/h/${pkgname}/${pkgname}-${pkgver}.tar.gz"
        "0001-add-files-for-autostart.patch")
sha512sums=('79f93bd8fa16180dd2789bb25a110bcdf793e1c583d8b7a05e54a811e2bc2ee4d166b0d6c02ad50a779c717ec64a902347a20787470013a8686a16e8ae241a66'
            '567624bbe2d5b8e45b6c1fac1629367602c380450f36869ae70eaaed4a6e3c7b9be400d6e2f53d5f1f63f755857eddf3a21c043ade2e6ebc69342e46f2028916')

prepare() {
  patch -d "$pkgname-$pkgver" -p1 -i "${srcdir}"/0001-add-files-for-autostart.patch
}

build() {
  cd "$pkgname-$pkgver"
  python -m build --wheel --no-isolation
}

package() {
  cd "$pkgname-$pkgver"
  python -m installer --destdir="$pkgdir" dist/*.whl
  
    # Install minimally necessary rules for running as a system service
    mkdir -p ${pkgdir}/usr/lib/udev/rules.d/
    mkdir -p ${pkgdir}/usr/lib/udev/hwdb.d/
    mkdir -p ${pkgdir}/usr/lib/systemd/system/
    mkdir -p ${pkgdir}/usr/share/polkit-1/actions/
    mkdir -p ${pkgdir}/etc/xdg/autostart/
    install -m775 usr/lib/enable-hhd ${pkgdir}/usr/lib/enable-hhd
    install -m775 etc/xdg/autostart/hhd.desktop ${pkgdir}/etc/xdg/autostart/hhd.desktop
    install -m644 usr/share/polkit-1/actions/org.hhd.start.policy ${pkgdir}/usr/share/polkit-1/actions/org.hhd.start.policy
    install -m644 usr/lib/udev/rules.d/83-hhd.rules ${pkgdir}/usr/lib/udev/rules.d/83-hhd.rules
    install -m644 usr/lib/udev/hwdb.d/83-hhd.hwdb ${pkgdir}/usr/lib/udev/hwdb.d/83-hhd.hwdb
    install -m644 usr/lib/systemd/system/hhd@.service ${pkgdir}/usr/lib/systemd/system/hhd@.service
}
